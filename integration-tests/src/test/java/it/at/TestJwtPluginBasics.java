package it.at;

import com.atlassian.jira.functest.framework.upm.DefaultCredentials;
import com.atlassian.jira.functest.framework.upm.UpmRestClient;
import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.pageobjects.TestedProductFactory;
import com.atlassian.test.categories.OnDemandAcceptanceTest;

import org.apache.http.auth.UsernamePasswordCredentials;
import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.util.concurrent.ExecutionException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

@Category(OnDemandAcceptanceTest.class)
public class TestJwtPluginBasics
{

    private static final String JWT_PLUGIN_KEY = "com.atlassian.jwt.jwt-plugin";

    private JiraTestedProduct jira = TestedProductFactory.create(JiraTestedProduct.class);

    private static UpmRestClient upmRestClient;

    @Before
    public void setUpUpmRestClient() throws Exception
    {
        final String baseUrl = jira.getProductInstance().getBaseUrl();
        upmRestClient = new UpmRestClient(baseUrl, getDefaultSysAdminCredentials());
    }

    @After
    public void tearDownUpmRestClient() throws Exception
    {
        if (upmRestClient != null) {
            upmRestClient.destroy();
        }
    }

    private UsernamePasswordCredentials getDefaultSysAdminCredentials()
    {
        final String username = System.getProperty("ondemand.username", "admin");
        final String password = System.getProperty("ondemand.password", "admin");
        return DefaultCredentials.getUsernamePasswordCredentials(username, password);
    }

    @Test
    public void testJwtAddonActive() throws Exception
    {
        assertThat("JWT plugin is not enabled", upmRestClient.requestPluginStatus(JWT_PLUGIN_KEY).get().isEnabled(), is(equalTo(true)));
    }
}
